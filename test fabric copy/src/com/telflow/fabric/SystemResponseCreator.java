package com.telflow.fabric;

import biz.dgit.schemas.telflow.cim.v3.Fault;
import biz.dgit.schemas.telflow.cim.v3.SystemMessage;
import biz.dgit.schemas.telflow.cim.v3.SystemMessageBody;
import biz.dgit.schemas.telflow.cim.v3.SystemResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class SystemResponseCreator {
	/**
     * Create a system response based on the body
     *
     * @param body to use as body
     * @return a {@link SystemResponse}
     */
    public SystemResponse toSystemResponse(final String body) {
        return createSystemResponse(body);
    }

    /**
     * Create a system response based on the body
     *
     * @param body to use as body
     * @return a {@link SystemResponse}
     */
    public SystemResponse toSystemResponse(final Throwable body) {
        final String stackTrace = ExceptionUtils.getStackTrace(body);
        return createSystemResponse(stackTrace).withException(new Fault().withCode(Integer.toString(-1))
            .withPublicMessage(stackTrace));
    }

    private static SystemResponse createSystemResponse(final String body) {
        return new SystemResponse().withSystemMessage(new SystemMessage().withBody(new SystemMessageBody().withData(
            body)));
    }

}
