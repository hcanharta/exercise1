package com.telflow.example.processforfabric;

import static org.junit.Assert.*;
import com.telflow.process.activiti.ForceFailActivitiRule;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Rule;
import org.junit.Test;

/**
 * Tests syntax of activiti diagram.
 */
public class Intro2ProcessTest {

    @Rule
    public ActivitiRule activitiRule = new ForceFailActivitiRule();

    @Test
    @Deployment(resources = {
        "OSGI-INF/activiti/intro2process.bpmn"
    })
    public void intro2processDeployment() {
        ProcessDefinitionQuery query = activitiRule.getRepositoryService().createProcessDefinitionQuery();
        ProcessDefinition definition = query.processDefinitionKey("testprocessfabric-v1").singleResult();
        assertNotNull(definition);
    }
}
